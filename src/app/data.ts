import { FrequencyInfoDTO } from './app.model';

export const data: FrequencyInfoDTO[] = [
    {
        frequnecyType: 'Daily',
        isResponsibilitySet: true,
        startFrom: null,
        endDetail: { key: 'END_AFTER', endByValue: null, endAfterValue: null },
        result: []
    },
    {
        frequnecyType: 'Weekly',
        isResponsibilitySet: true,
        startFrom: null,
        endDetail: { key: 'END_AFTER', endByValue: null, endAfterValue: null },
        result: []
    },
    {
        frequnecyType: 'Monthly',
        isResponsibilitySet: true,
        startFrom: null,
        endDetail: { key: 'END_AFTER', endByValue: null, endAfterValue: null },
        result: []
    },
    {
        frequnecyType: 'Yearly',
        isResponsibilitySet: true,
        startFrom: null,
        endDetail: { key: 'END_AFTER', endByValue: null, endAfterValue: null },
        result: []
    },
    {
        frequnecyType: 'OneTime',
        isResponsibilitySet: true,
        startFrom: null,
        endDetail: { key: 'END_AFTER', endByValue: null, endAfterValue: null },
        result: []
    }
];
