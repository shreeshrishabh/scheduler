import { Component, OnInit, Input, SimpleChanges, OnChanges, ChangeDetectorRef } from '@angular/core';
import { FrequencyInfoDTO } from '../app.model';

@Component({
  selector: 'app-frequency',
  templateUrl: './frequency.component.html',
  styleUrls: ['./frequency.component.css']
})
export class FrequencyComponent implements OnInit, OnChanges {
  @Input() isSelect = '';
  @Input() selectedObj = new FrequencyInfoDTO();

  constructor(private ref: ChangeDetectorRef) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.selectedObj = changes.selectedObj.currentValue;
  }
}
