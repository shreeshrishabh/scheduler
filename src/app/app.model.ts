export class FrequencyInfoDTO {
    frequnecyType = '';
    isResponsibilitySet = true;
    startFrom: Date = null;
    endDetail: { key: string, endByValue: string, endAfterValue: number } = { key: '', endByValue: null, endAfterValue: 0 };
    result?: string[] = [];
}

// export class FrequencyDetails {
//     startFrom: Date = null;
//     endDetail: { key: string, value: string } = { key: '', value: '' };
// }

export class HolidayList {
    holidayName = '';
    date: string = null;
}
