import { Component, OnInit } from '@angular/core';
import { data } from './data';
import { FrequencyInfoDTO, HolidayList } from './app.model';
import { MatTableDataSource } from '@angular/material';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'scheduler';
  displayedColumns: string[] = ['holidayName', 'date'];
  oneTimeDate: Date = null;
  selectedMonthlyDate = 23;
  selectedYearly = 'Jan';
  selectedYearlyDate = 23;
  frequencyData = data;
  yearlyList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  weeklyList = [{ key: 'Mon', value: true },
  { key: 'Tue', value: true },
  { key: 'Wed', value: true },
  { key: 'Thur', value: true },
  { key: 'Fri', value: true },
  { key: 'Sat', value: true },
  { key: 'Sun', value: true }];
  monthlyList = [{ key: 'Jan', value: true },
  { key: 'Feb', value: true },
  { key: 'Mar', value: true },
  { key: 'Apr', value: true },
  { key: 'May', value: true },
  { key: 'Jun', value: true },
  { key: 'July', value: true },
  { key: 'Aug', value: true },
  { key: 'Sep', value: true },
  { key: 'Oct', value: true },
  { key: 'Nov', value: true },
  { key: 'Dec', value: true }];
  holidayWeekList = [{ key: 'Mon', value: false, vNumber: 0 },
  { key: 'Tue', value: false, vNumber: 1 },
  { key: 'Wed', value: false, vNumber: 2 },
  { key: 'Thur', value: false, vNumber: 3 },
  { key: 'Fri', value: false, vNumber: 4 },
  { key: 'Sat', value: true, vNumber: 5 },
  { key: 'Sun', value: true, vNumber: 6 }];
  holidayData: HolidayList[] = [];
  dataSource = new MatTableDataSource(this.holidayData);
  holidayName = '';
  holidayDate: Date = null;
  selectedTab = 0;
  ngOnInit() {
  }


  getFrequencySelectedList(frequnecyType) {
    return this.frequencyData.filter(a => a.frequnecyType === frequnecyType)[0];
  }

  getdata() {
    for (const item of this.frequencyData) {
      if (item.frequnecyType === 'Daily') {
        this.calculateDailyFrequency(item);
      } else if (item.frequnecyType === 'Weekly') {
        this.calculateWeeklyFrequency(item);
      } else if (item.frequnecyType === 'Monthly') {
        this.calculateMonthlyFrequency(item);
      } else if (item.frequnecyType === 'Yearly') {
        this.calculateYearlyFrequency(item);
      } else if (item.frequnecyType === 'OneTime') {
        this.calculateOneTimeFrequency(item);
      }
    }
    this.selectedTab = 6;
  }

  calculateDailyFrequency(item: FrequencyInfoDTO) {
    const holidaySelectedList = this.holidayWeekList.filter(a => a.value === true).map((mo) => mo.vNumber);
    const holidayDateList = this.holidayData.map((h) => h.date);
    const date = new Date(item.startFrom);
    if (item.endDetail.key === 'END_AFTER') {
      for (let item1 = 0; item1 < item.endDetail.endAfterValue; item1++) {
        const d1 = new Date(date);
        d1.setDate(date.getDate() + (item1));
        const day = d1.getDay();
        if (!holidaySelectedList.includes(day)) {
          if (!holidayDateList.includes(this.computeDate(d1))) {
            item.result.push(this.computeDate(d1));
          }
        }
      }
    } else if (item.endDetail.key === 'END_BY') {
      const dateEndBy = new Date(item.endDetail.endByValue);
      let preDate = date;
      let i = 0;
      while (preDate < dateEndBy) {
        const d1 = new Date(date);
        d1.setDate(date.getDate() + i);
        const day = d1.getDay();
        if (!holidaySelectedList.includes(day)) {
          if (!holidayDateList.includes(this.computeDate(d1))) {
            item.result.push(this.computeDate(d1));
          }
        }
        preDate = d1;
        i++;
      }
    }
  }

  calculateWeeklyFrequency(item: FrequencyInfoDTO) {
    const list = this.weeklyList.filter(a => a.value === true);
    const holidaySelectedList = this.holidayWeekList.filter(a => a.value === true).map((mo) => mo.vNumber);
    const holidayDateList = this.holidayData.map((h) => h.date);
    if (list.length !== 0) {
      for (const week of list) {
        const weekNumber = this.getWeekNumber(week.key);
        const date = new Date(item.startFrom);
        const tempdate = date.setDate(date.getDate() + (weekNumber + (7 - date.getDay())) % 7);
        const tempConvertDate = new Date(tempdate);
        if (item.endDetail.key === 'END_AFTER') {
          for (let item1 = 0; item1 < item.endDetail.endAfterValue / list.length; item1++) {
            const d1 = new Date(tempConvertDate);
            d1.setDate(tempConvertDate.getDate() + (item1 * 7));
            const day = d1.getDay();
            if (!holidaySelectedList.includes(day)) {
              if (!holidayDateList.includes(this.computeDate(d1))) {
                item.result.push(this.computeDate(d1));
              }
            }
          }
        } else if (item.endDetail.key === 'END_BY') {
          const dateEndBy = new Date(item.endDetail.endByValue);
          let i = 0;
          let preDate = tempConvertDate;
          while (preDate < dateEndBy) {
            const d1 = new Date(tempConvertDate);
            d1.setDate(tempConvertDate.getDate() + (i * 7));
            preDate = d1;
            if (preDate < dateEndBy) {
              const day = d1.getDay();
              if (!holidaySelectedList.includes(day)) {
                if (!holidayDateList.includes(this.computeDate(d1))) {
                  item.result.push(this.computeDate(d1));
                }
              }
            }
            i++;
          }
        }
      }
    }
  }


  calculateMonthlyFrequency(item: FrequencyInfoDTO) {
    const holidaySelectedList = this.holidayWeekList.filter(a => a.value === true).map((mo) => mo.vNumber);
    const holidayDateList = this.holidayData.map((h) => h.date);
    const date = new Date(item.startFrom);
    let preDate = date;
    let isvalid = false;
    if (item.startFrom === null) {
      isvalid = false;
    }
    const list = this.monthlyList.filter(a => a.value === true).map((mo) => mo.key);
    if (list.length !== 0) {
      if (item.endDetail.key === 'END_AFTER') {
        while (isvalid) {
          const tempdate = preDate.setMonth(preDate.getMonth() + 1, this.selectedMonthlyDate);
          const tempConvertDate = new Date(tempdate);
          const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
          const mm = monthNames[tempConvertDate.getMonth()];
          if (list.includes(mm)) {
            const d1 = new Date(tempConvertDate.getFullYear(), tempConvertDate.getMonth(), this.selectedMonthlyDate);
            const day = d1.getDay();
            if (!holidaySelectedList.includes(day)) {
              if (!holidayDateList.includes(this.computeDate(d1))) {
                item.result.push(this.computeDate(d1));
              }
            }
          }
          if (item.result.length == item.endDetail.endAfterValue) {
            isvalid = false;
          } else {
            isvalid = true;
          }
          preDate = tempConvertDate;
        }
      } else if (item.endDetail.key === 'END_BY') {
        const dateEndBy = new Date(item.endDetail.endByValue);
        while (isvalid) {
          const tempdate = preDate.setMonth(preDate.getMonth() + 1, this.selectedMonthlyDate);
          const tempConvertDate = new Date(tempdate);
          const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
          const mm = monthNames[tempConvertDate.getMonth()];
          const d1 = new Date(tempConvertDate.getFullYear(), tempConvertDate.getMonth(), this.selectedMonthlyDate);
          if (list.includes(mm)) {
            const day = d1.getDay();
            if (!holidaySelectedList.includes(day)) {
              if (!holidayDateList.includes(this.computeDate(d1))) {
                item.result.push(this.computeDate(d1));
              }
            }
          }
          if (preDate < dateEndBy) {
            isvalid = true;
          } else {
            isvalid = false;
          }
          preDate = d1;
        }
      }
    }
  }


  calculateYearlyFrequency(item: FrequencyInfoDTO) {
    const holidaySelectedList = this.holidayWeekList.filter(a => a.value === true).map((mo) => mo.vNumber);
    const holidayDateList = this.holidayData.map((h) => h.date);
    const date = new Date(item.startFrom);
    if (item.endDetail.key === 'END_AFTER') {
      for (let item1 = 0; item1 < item.endDetail.endAfterValue; item1++) {
        const d1 = new Date(date.getFullYear() + (item1 + 1), this.getMonthNumber(this.selectedYearly), this.selectedYearlyDate);
        const day = d1.getDay();
        if (!holidaySelectedList.includes(day)) {
          if (!holidayDateList.includes(this.computeDate(d1))) {
            item.result.push(this.computeDate(d1));
          }
        }
      }
    } else if (item.endDetail.key === 'END_BY') {
      let i = 0;
      let preDate = date;
      const dateEndBy = new Date(item.endDetail.endByValue);
      while (preDate < dateEndBy) {
        const d1 = new Date(date.getFullYear() + (i + 1), this.getMonthNumber(this.selectedYearly), this.selectedYearlyDate);
        const day = d1.getDay();
        if (!holidaySelectedList.includes(day)) {
          if (!holidayDateList.includes(this.computeDate(d1))) {
            item.result.push(this.computeDate(d1));
          }
        }
        preDate = d1;
        i++;
      }
    }
  }

  calculateOneTimeFrequency(item: FrequencyInfoDTO) {
    item.result = [];
    if (this.oneTimeDate !== null) {
      const date = new Date(this.oneTimeDate);
      item.result.push(this.computeDate(date));
    }
  }


  computeDate(date: Date) {
    let dd = date.getDate();
    const mm = date.getMonth() + 1;
    const yyyy = date.getFullYear();
    if (dd < 10) { dd = +'0' + dd; }
    return yyyy + '-' + mm + '-' + dd;
  }

  computeDate2(date: Date) {
    let dd = date.getDate();
    const mm = date.getMonth();
    const yyyy = date.getFullYear();
    if (dd < 10) { dd = +'0' + dd; }
    return yyyy + '-' + mm + '-' + dd;
  }

  getWeekNumber(item) {
    if (item === 'Mon') {
      return 1;
    } else if (item === 'Tue') {
      return 2;
    } else if (item === 'Wed') {
      return 3;
    } else if (item === 'Thur') {
      return 4;
    } else if (item === 'Fri') {
      return 5;
    } else if (item === 'Sat') {
      return 6;
    } else if (item === 'Sun') {
      return 7;
    }
  }
  getMonthNumber(item) {
    if (item === 'Jan') {
      return 1;
    } else if (item === 'Feb') {
      return 2;
    } else if (item === 'Mar') {
      return 3;
    } else if (item === 'Apr') {
      return 4;
    } else if (item === 'May') {
      return 5;
    } else if (item === 'Jun') {
      return 6;
    } else if (item === 'July') {
      return 7;
    } else if (item === 'Aug') {
      return 8;
    } else if (item === 'Sep') {
      return 9;
    } else if (item === 'Oct') {
      return 10;
    } else if (item === 'Nov') {
      return 11;
    } else if (item === 'Dec') {
      return 12;
    }
  }

  getWeek() {
    let returnValue = '';
    const list = this.weeklyList.filter(a => a.value === true);
    if (list.length !== 0) {
      for (const week of list) {
        returnValue = returnValue + this.getWeekNumber(week.key) + ', ';
      }
    }
    return returnValue;
  }

  saveHoliday() {
    const date = new Date(this.holidayDate);
    this.holidayData.push({ holidayName: this.holidayName, date: this.computeDate(date) });
    this.dataSource.data = this.holidayData;
  }
}
